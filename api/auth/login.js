export default class ApiAuthLogin {
  constructor(axios) {
    this.axios = axios
  }

  fetchOauthUrl(provider) {
    return this.axios.post('/oauth/' + provider)
  }

  login(form) {
    return this.axios.post('/login', form)
  }

  logout() {
    return this.axios.post('/logout')
  }
}
