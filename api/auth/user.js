export default class ApiAuthUser {
  constructor(axios) {
    this.axios = axios
  }

  fetchUser() {
    return this.axios.get('/user')
  }
}
