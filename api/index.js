import ApiAuth from '~/api/api.auth'
import ApiAlbum from '~/api/api.album'

export default class Api {
  constructor(axios) {
    this.auth = new ApiAuth(axios)
    this.album = new ApiAlbum(axios)
  }
}
