export default class ApiAlbum {
  constructor (axios, base_url) {
    this.axios = axios
    this.url = '/album'
  }

  create (album) {
    return this.axios.post(this.url, album)
  }

  findAll () {
    return this.axios.get(this.url)
  }

  find (search) {
    const params = search
    return this.axios.get(this.url + '/find', { params })
  }

  remove (id) {
    return this.axios.delete(this.url + '/' + id)
  }

  update (album) {
    return this.axios.put(this.url + '/' + album.id, album)
  }
}
