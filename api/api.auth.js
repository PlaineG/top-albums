import ApiAuthLogin from '~/api/auth/login'
import ApiAuthUser from '~/api/auth/user'

export default class ApiAuth {
  constructor(axios) {
    this.login = new ApiAuthLogin(axios)
    this.user = new ApiAuthUser(axios)
  }
}
