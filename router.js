import Vue from 'vue'
import Router from 'vue-router'
import { scrollBehavior } from '~/utils'

Vue.use(Router)

const page = (path) => () =>
  import(`~/pages/${path}`).then((m) => m.default || m)

const routes = [
  { path: '/', name: 'home', component: page('home.vue') },
  { path: '/login', name: 'login', component: page('auth/login.vue') },
  { path: '/form', name: 'form', component: page('form.vue') },
  { path: '/info', name: 'info', component: page('info.vue') },
]

export function createRouter() {
  return new Router({
    routes,
    scrollBehavior,
    mode: 'history',
  })
}
