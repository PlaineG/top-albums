import axios from 'axios'
// import _ from 'lodash'

export default async ({ store, req, route, redirect }) => {
  const token = store.getters['auth/token']

  if (!store.getters['auth/check'] && !token && route.name !== 'login') {
    return redirect('/login')
  }

  if (process.server) {
    if (token) {
      axios.defaults.headers.common.Authorization = `Bearer ${token}`
    } else {
      delete axios.defaults.headers.common.Authorization
    }
  }

  if (!store.getters['auth/check'] && token) {
    await store.dispatch('auth/fetchUser')
  }
}
