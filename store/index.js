import Cookies from 'js-cookie'
import { cookieFromRequest } from '~/utils'

export const actions = {
  nuxtServerInit({ commit }, { req }) {
    const token = cookieFromRequest(req, 'token')
    if (token) {
      commit('auth/setToken', token)
    }

    const locale = cookieFromRequest(req, 'locale')
    if (locale) {
      commit('lang/setLocale', { locale })
    }
  },

  nuxtClientInit({ commit }) {
    const token = Cookies.get('token')
    if (token) {
      commit('auth/setToken', token)
    }

    const locale = Cookies.get('locale')
    if (locale) {
      commit('lang/setLocale', { locale })
    }
  },
}
