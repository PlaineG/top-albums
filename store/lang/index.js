import Cookies from 'js-cookie'

// state
const state = () => ({
  locale: process.env.appLocale,
  locale_array: [
    { id: 'fr', value: 'FR' },
    { id: 'en', value: 'EN' }
  ],
})

// getters
const getters = {
  locale: (state) => state.locale,
  locale_array: (state) => state.locale_array,
}

// actions
const actions = {
  setLocale({ commit }, { locale }) {
    commit('setLocale', { locale })

    Cookies.set('locale', locale, { expires: 365 })
  },
}

// mutations
const mutations = {
  setLocale(state, { locale }) {
    state.locale = locale
  },
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
}
