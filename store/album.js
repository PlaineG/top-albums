import _ from 'lodash'

// State ------------------------------------------------------------------------------------------

const state = () => ({
  album: {},
  album_array: []
})

// Getters ----------------------------------------------------------------------------------------

const getters = {
  album: (state) => state.album,
  album_array: (state) => state.album_array
}

// Actions ----------------------------------------------------------------------------------------

const actions = {
  async create ({ commit }, album) {
    const response = await this.$api.album.create(album)
    commit('create', response.data.data[0])
  },

  async findAll ({ commit }) {
    const response = await this.$api.album.findAll()
    commit('setAlbumArray', response.data.data)
  },

  async find ({ commit }, search) {
    const response = await this.$api.album.find(search)
    commit('setAlbum', response.data.data)
  },

  async remove ({ commit }, id) {
    await this.$api.album.remove(id)
    commit('remove', id)
  },  

  async update ({ commit }, album) {
    const response = await this.$api.album.update(album)
    commit('update', response.data.data[0])
  },

  vote ({ commit }, album) {
    const album_clone = _.cloneDeep(album)
    album_clone.vote++

    this.$api.album.update(album_clone)

    commit('vote', album_clone)
  }
}

// Mutations --------------------------------------------------------------------------------------

const mutations = {
  create (state, album) {
    state.album_array.push(album)
  },

  remove (state, id) {
    const album_array = _.cloneDeep(state.album_array)
    state.album_array = _.reject(
      album_array,
      a => a.id === id
    )
  },

  reset (state) {
    state.album = {}
  },

  setAlbumArray (state, album_array) {
    state.album_array = album_array
  },

  setAlbum (state, album) {
    state.album = album
  },

  update (state, album) {
    const album_array = _.cloneDeep(state.album_array)

    const idx = _.findIndex(
      album_array,
      a => a.id === album.id
    )

    album_array[idx] = album
    state.album_array = album_array
  },

  vote (state, album) {
    const album_array = _.cloneDeep(state.album_array)

    const idx = _.findIndex(
      album_array,
      a => a.id === album.id
    )

    album_array[idx] = album
    state.album_array = _.orderBy(album_array, ['vote'], ['desc'])
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
}
