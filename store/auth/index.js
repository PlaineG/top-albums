import Cookies from 'js-cookie'

// State ------------------------------------------------------------------------------------------

const state = () => ({
  user: null,
  token: null,
})

// Getters ----------------------------------------------------------------------------------------

const getters = {
  user: (state) => state.user,
  token: (state) => state.token,
  check: (state) => state.user !== null,
}

// Actions ----------------------------------------------------------------------------------------

const actions = {
  async fetchOauthUrl(ctx, { provider }) {
    const { data } = await this.$api.auth.login.fetchOauthUrl(provider)

    return data.url
  },

  async fetchUser({ commit }) {
    try {
      const { data } = await this.$api.auth.user.fetchUser()

      commit('fetchUserSuccess', data.data)
    } catch (e) {
      Cookies.remove('token')

      commit('fetchUserFailure')
    }
  },

  async login({ commit }, form) {
    return await this.$api.auth.login.login(form)
  },

  async logout({ commit }) {
    try {
      await this.$api.auth.login.logout()
    } catch (e) {}

    Cookies.remove('token')

    commit('logout')
  },

  saveToken({ commit, dispatch }, { token, remember }) {
    commit('setToken', token)

    Cookies.set('token', token, { expires: remember ? 365 : null })
  },

  updateUser({ commit }, payload) {
    commit('updateUser', payload)
  },
}

// Mutations --------------------------------------------------------------------------------------

const mutations = {
  setToken(state, token) {
    state.token = token
  },

  fetchUserSuccess(state, user) {
    state.user = user
  },

  fetchUserFailure(state) {
    state.token = null
  },

  logout(state) {
    state.user = null
    state.token = null
  },

  updateUser(state, { user }) {
    state.user = user
  },
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
}
