// State ------------------------------------------------------------------------------------------

const state = () => ({
  show: false,
  type: '',
  message: '',
  timeout: 2000,
})

// Getters ----------------------------------------------------------------------------------------

const getters = {
  show: (state) => state.show,
  type: (state) => state.type,
  message: (state) => state.message,
  timeout: (state) => state.timeout,
}

// Actions ----------------------------------------------------------------------------------------

const actions = {
  hide({ commit }) {
    commit('hide')
  },

  show({ commit }, param) {
    commit('show', param)
  },
}

// Mutations --------------------------------------------------------------------------------------

const mutations = {
  hide(state) {
    state.show = false
  },

  show(state, param) {
    state.type = param.type
    state.message = param.message
    state.timeout = this.$tools.isset(param.timeout) ? param.timeout : 4000
    state.show = true
  },
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
}
