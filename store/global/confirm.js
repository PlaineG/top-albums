// State ------------------------------------------------------------------------------------------

const state = () => ({
  show: false,
  message: '',
  resolve: null,
  reject: null,
})

// Getters ----------------------------------------------------------------------------------------

const getters = {}

// Actions ----------------------------------------------------------------------------------------

const actions = {
  ask({ commit }, message) {
    return new Promise((resolve, reject) => {
      commit('accept', { message, resolve, reject })
    })
  },
}

// Mutations --------------------------------------------------------------------------------------

const mutations = {
  accept(state, payload) {
    state.show = true
    state.message = payload.message
    state.resolve = payload.resolve
    state.reject = payload.reject
  },

  cancel(state) {
    state.show = false
    state.message = ''
    state.resolve = null
    state.reject = null
  },
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
}
