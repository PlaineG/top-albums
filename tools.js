import moment from 'moment'

export function isset(val) {
  return typeof val !== 'undefined' && val !== null && val !== '' && val !== {}
}

export function formatDate(date) {
  return this.isset(date) ? moment(date, 'YYYY-MM-DD').format('DD/MM/YYYY') : ''
}

export function formatDateFull(date) {
  return this.isset(date)
    ? moment(date, 'YYYY-MM-DD HH:mm').format('DD/MM/YYYY HH:mm')
    : ''
}

export function generateFile(file) {
  const fileLink = document.createElement('a')

  fileLink.href = file.filepath
  fileLink.setAttribute('download', file.filename)
  fileLink.setAttribute('target', '_blank')
  document.body.appendChild(fileLink)

  fileLink.click()
}
