import Vue from 'vue'
import GInput from '@/components/inputs/GInput'
import GTextarea from '@/components/inputs/GTextarea'
    
const components = { GInput, GTextarea }
   
Object.entries(components).forEach(([name, component]) => {
    Vue.component(name, component)
})