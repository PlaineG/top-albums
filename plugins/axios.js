import axios from 'axios'
import _ from 'lodash'

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'

export default ({ app, store, redirect }) => {
  axios.defaults.baseURL = process.env.apiUrl

  if (process.server) {
    return
  }

  // Request interceptor
  axios.interceptors.request.use((request) => {
    request.baseURL = process.env.apiUrl

    const token = store.getters['auth/token']

    if (token) {
      request.headers.common.Authorization = `Bearer ${token}`
    }

    const locale = store.getters['lang/locale']
    if (locale) {
      request.headers.common['Accept-Language'] = locale
    }

    return request
  })

  // Response interceptor
  axios.interceptors.response.use(
    (response) => {
      if (
        (response.config.method === 'post' ||
          response.config.method === 'put') &&
        response.status === 200 &&
        response.config.url !== '/login' &&
        response.config.url !== '/logout'
      ) {
        store.dispatch('global/snackbar/show', {
          type: 'success',
          message: 'Enregistrement réussi.',
        })
      }

      if (response.config.method === 'delete' && response.status === 200) {
        store.dispatch('global/snackbar/show', {
          type: 'success',
          message: 'Suppression réussie.',
        })
      }

      if (
        response.config.method === 'get' &&
        response.data.message.warning_array.length > 0
      ) {
        store.dispatch('global/snackbar/show', {
          type: 'warning',
          message: response.data.message.warning_array[0].message,
        })
      }

      return Promise.resolve(response)
    },
    (error) => {
      const { status } = error.response || {}

      if (status === 500) {
        store.dispatch('global/snackbar/show', {
          type: 'error',
          message: error.response.data.message,
          timeout: 6000,
        })
      }

      if (status === 503) {
        store.dispatch('global/snackbar/show', {
          type: 'error',
          message: 'Accès non autorisé, veuillez-vous reconnecter.',
        })
      }

      if (status === 422) {
        let error_text = 'Les données fournies ne sont pas valides :'
        _.forEach(error.response.data.errors, (e) => {
          error_text = error_text + '<br> - ' + e[0]
        })
        store.dispatch('global/snackbar/show', {
          type: 'error',
          message: error_text,
        })
      }

      if (status === 404) {
        redirect('/404')
      }

      if (status === 401 && store.getters['auth/check']) {
        swal({
          type: 'warning',
          title: app.i18n.t('token_expired_alert_title'),
          text: app.i18n.t('token_expired_alert_text'),
          reverseButtons: true,
          confirmButtonText: app.i18n.t('ok'),
          cancelButtonText: app.i18n.t('cancel'),
        }).then(() => {
          store.commit('auth/LOGOUT')

          redirect({ name: 'login' })
        })
      }

      return Promise.reject(error)
    }
  )
}
