import * as tools from '~/tools'

export default (ctx, inject) => {
  inject('tools', tools)
}
