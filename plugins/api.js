import axios from 'axios'

import Api from '~/api'

export default (ctx, inject) => {
  const api = new Api(axios)
  inject('api', api)
}
